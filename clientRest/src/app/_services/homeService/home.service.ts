import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";


@Injectable()
export class HomeService {
    baseUrl = environment.apiUrl; 
  constructor(private httpClient: HttpClient) { }

//   addUser(user: User) {
//     return this.httpClient.post('/api/Users', user);
//   }

  loginUserLinkedIn(): Observable<any> {
    const headers = new HttpHeaders().append('Access-Control-Allow-Origin', ['*']); 
    return this.httpClient.get<any>(this.baseUrl + 'home/signInLinkedIn', { headers });
  }
}