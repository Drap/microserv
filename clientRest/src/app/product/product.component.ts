import { Component, OnInit } from '@angular/core';
import { ProductService } from '../_services/productService/product.service';
import { Product } from '../_models/product';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  products: Product[];

  constructor(private ProductService : ProductService) { }

  ngOnInit(): void {
    this.LoadProduct();
  }

  LoadProduct(){
      this.ProductService.GetProducts().subscribe((data: any )=> {
        this.products = data.result;
        console.log(this.products)
      })

  } 

}
