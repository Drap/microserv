import { Component, OnInit } from '@angular/core';
import { HomeService } from '../_services/homeService/home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})


export class HomeComponent implements OnInit {


  ngOnInit(): void {
  }

  constructor(
    private homeService: HomeService) { }

  OnLinkedInLoad() {
    this.homeService.loginUserLinkedIn().subscribe()
  }

}
