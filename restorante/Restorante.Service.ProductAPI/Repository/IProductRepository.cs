﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Restorante.Service.ProductAPI.Models.Dto;

namespace Restorante.Service.ProductAPI.Repository
{
    public interface IProductRepository
    {
        Task<IEnumerable<ProductDto>> GetProducts();
        Task<ProductDto> GetProductById(int productId);
        Task<ProductDto> CreateUpdateProduct(ProductDto productDto);
        Task<bool> DeleteProduct(int productId);
    }
}
