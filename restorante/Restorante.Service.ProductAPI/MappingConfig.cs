﻿using System;
using AutoMapper;
using Restorante.Service.ProductAPI.Models;
using Restorante.Service.ProductAPI.Models.Dto;

namespace Restorante.Service.ProductAPI
{
    public class MappingConfig
    {
        public static MapperConfiguration RegisterMaps()
        {
            var mappingConfig = new MapperConfiguration(config =>
            {
                config.CreateMap<ProductDto, Product>();
                config.CreateMap<Product, ProductDto>();
            });

            return mappingConfig;

        }
    }
}
