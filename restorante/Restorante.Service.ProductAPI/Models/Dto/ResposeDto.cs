﻿using System;
using System.Collections.Generic;

namespace Restorante.Service.ProductAPI.Models.Dto
{
    public class ResponseDto
    {
        public bool Issuccess { get; set; } = true;
        public object Result { get; set; } 
        public string DisplayMessage { get; set; } = "";
        public List<String> ErrorsMessages { get; set; } 

    }
}
