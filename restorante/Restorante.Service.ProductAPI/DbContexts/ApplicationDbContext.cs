﻿using System;
using Microsoft.EntityFrameworkCore;
using Restorante.Service.ProductAPI.Models;

namespace Restorante.Service.ProductAPI.DbContexts
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Product> Products { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelbuilder)
        //{

        //}
    }
}
