﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Restorante.Service.ProductAPI.Models.Dto;
using Restorante.Service.ProductAPI.Repository;
using Newtonsoft.Json;

namespace Restorante.Service.ProductAPI.Controllers
{

    [Route("api/products")]

    public class ProductAPIController : ControllerBase
    {

        protected ResponseDto _response;
        private IProductRepository _productRepository;

        public ProductAPIController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
            this._response = new ResponseDto();
        }

        [HttpGet]
        //[Route("{id}")]
        public async Task<object> Get()
        {
            //string baseAddress = @"https://www.linkedin.com/oauth2/token";

            //string grant_type = "client_credentials";
            //string client_id = "861qxnmp4jooiu";
            //string client_secret = "gMANjhFKCepfoMRs";

            //var form = new Dictionary<string, string>
            //    {
            //        {"grant_type", grant_type},
            //        {"client_id", client_id},
            //        {"client_secret", client_secret},
            //    };

            //HttpResponseMessage tokenResponse = await client.PostAsync(baseAddress, new FormUrlEncodedContent(form));
            //var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
            //Token tok = JsonConvert.DeserializeObject<Token>(jsonContent);
            //return tok;
            try
            {
                IEnumerable<ProductDto> productDtos = await _productRepository.GetProducts();
                _response.Result = productDtos;

            }
            catch (Exception ex)
            {
                _response.Issuccess = false;
                _response.ErrorsMessages = new List<string>() { ex.ToString() };
            }

            return _response;
        }


        [HttpGet]
        [Route("{id}")]
        public async Task<object> Get(int id)
        {
            try
            {
                ProductDto productDto = await _productRepository.GetProductById(id);
                _response.Result = productDto;
            }
            catch (Exception ex)
            {
                _response.Issuccess = false;
                _response.ErrorsMessages = new List<string>() { ex.ToString() };
            }

            return _response;
        }

        [HttpPost]

        public async Task<object> Post([FromBody] ProductDto productDto)
        {
            try
            {
                ProductDto model = await _productRepository.CreateUpdateProduct(productDto);
                _response.Result = model;
            }
            catch (Exception ex)
            {
                _response.Issuccess = false;
                _response.ErrorsMessages = new List<string>() { ex.ToString() };
            }

            return _response;
        }


        [HttpPut]

        public async Task<object> Put([FromBody] ProductDto productDto)
        {
            try
            {
                ProductDto model = await _productRepository.CreateUpdateProduct(productDto);
                _response.Result = model;
            }
            catch (Exception ex)
            {
                _response.Issuccess = false;
                _response.ErrorsMessages = new List<string>() { ex.ToString() };
            }

            return _response;
        }


        [HttpDelete]

        public async Task<object> Delete(int id)
        {
            try
            {
                bool isSuccess = await _productRepository.DeleteProduct(id);
                _response.Result = isSuccess;
            }
            catch (Exception ex)
            {
                _response.Issuccess = false;
                _response.ErrorsMessages = new List<string>() { ex.ToString() };
            }

            return _response;
        }


        //[HttpGet]
        ////[Route("Token")]

        //private static async Task<Token> GetToken(HttpClient client)
        //{
        //    string baseAddress = @"https://www.linkedin.com/oauth2/token";

        //    string grant_type = "client_credentials";
        //    string client_id = "861qxnmp4jooiu";
        //    string client_secret = "gMANjhFKCepfoMRs";

        //    var form = new Dictionary<string, string>
        //        {
        //            {"grant_type", grant_type},
        //            {"client_id", client_id},
        //            {"client_secret", client_secret},
        //        };

        //    HttpResponseMessage tokenResponse = await client.PostAsync(baseAddress, new FormUrlEncodedContent(form));
        //    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
        //    Token tok = JsonConvert.DeserializeObject<Token>(jsonContent);
        //    return tok;
        //}

        internal class Token
        {
            [JsonProperty("access_token")]
            public string AccessToken { get; set; }

            [JsonProperty("token_type")]
            public string TokenType { get; set; }

            [JsonProperty("expires_in")]
            public int ExpiresIn { get; set; }

            [JsonProperty("refresh_token")]
            public string RefreshToken { get; set; }
        }

    }
}
