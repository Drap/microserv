﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;

namespace Restorante.Service.ProductAPI.Controllers
{
    [Route("api/home")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        //public HomeController(LoginTestContext context)
        //{
        //}

        [HttpGet("signInLinkedIn")]
        public async Task LinkedInSignInAsync()
        {
            //https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=861qxnmp4jooiu&scope=r_liteprofile&state=123456&redirect_uri=http://localhost:4200

            //AQTBJ4OmWBDrnwZ69 - mueVm2wUf3FddFwoopd04GNFOsQOS8C4cDP170Z6MkkbAto6dj3 - cDdWyfAh5yWxUORzZnDsS3rvkBz35pPKuSpn4r - Lf5XXCkb3QKlQevnM9iw7KGgAGFXHIFo - SMU7yxuuUcZ9B0vmmRHoShLmXsmn8MPi8RISXdtc5xqQEjkyEPRuGpM_8MUCzFVpCsZyM


            //https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&client_id=861qxnmp4jooiu&client_secret=gMANjhFKCepfoMRs&code=AQTdqca1AAWJpnwdF5Lm_2B1LdjTVfIWLxVUtJZd3xAinEnccAiFBQpkY2_yytq9bF8j6NSvPKvgZdoFa-bFQlCKCAmb8Xf9PM5ST2Z7ndI5UutKsLPha2tO6CkdvizQGIZJaMa-g3E3HVfS5SLSUpcriexI_DOBjiHXs1ctDbiALwUDbwoZXtyF05AxUiCKzrvg0UuSzgRhy1ug414
            //&redirect_uri = http://localhost:4200

            //await HttpContext.ChallengeAsync("linkedin", new AuthenticationProperties() { RedirectUri = "/" });
            //Response.Redirect("https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=861qxnmp4jooiu&redirect_uri=https%3A%2F%2Fdev.example.com%2Fauth%2Flinkedin%2Fcallback&state=fooobar&scope=r_liteprofile%20r_emailaddress%20w_member_social");
            Response.Redirect("https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=861qxnmp4jooiu&scope=r_liteprofile&state=123456&redirect_uri=http://localhost:4200");
        }
    }
}
